hydrotoolbox
================

<!-- badges: start -->

[![CRAN
release](https://www.r-pkg.org/badges/version/hydrotoolbox?color=orange)](https://cran.r-project.org/package=hydrotoolbox)
[![monthly\_download](http://cranlogs.r-pkg.org/badges/last-month/hydrotoolbox?color=green)](https://cran.r-project.org/package=hydrotoolbox)
[![lifecycle](https://img.shields.io/badge/lifecycle-maturing-blue.svg)](https://www.tidyverse.org/lifecycle/#maturing)
[![License: GPL
(\>= 3)](https://img.shields.io/badge/license-GPL%20\(%3E=%203\)-forestgreen.svg)](https://cran.r-project.org/web/licenses/GPL%20\(%3E=%203\))
<!-- badges: end -->

## Overview

`hydrotookbox` contains objects, methods and functions to efficiently
manage hydro-meteorological data sets. The goal is to provide easy to
use functionality to read, plot, manipulate and process
hydro-meteorological data records and also for pre and post processing
data for hydrological modeling. The package has special features for
Argentina and Chile data-sets (SNHI, CR2, IANIGLA, AIC and DGI).

The goal is to provide general objects and methods to be used with any
hydro-meteorological data set for:

  - condense all the information that a station records in a single
    object.
  - easily plot (dynamic and static graphs) the series for data
    analysis, reporting and publishing.
  - modify the original series in order to remove all kind of
    mistrustful periods without loosing the original data.
  - pre and post process series for hydrological modeling.

For example, creating an `hydromet_station` object and storing the
series just takes a few lines:

``` r
library(hydrotoolbox)

path <- system.file('extdata', package = 'hydrotoolbox')

# we first build the station object 
guido <- 
  hm_create() %>%
  hm_build(bureau = 'mnemos', path = path, 
           file_name = 'mnemos_guido.xlsx', 
           slot_name = c('qd', 'evap', 'tair',
                         'tmax', 'tmin', 'wspd'), 
           by = c('day', 'day', '6 hour', 
                  'day', 'day', '6 hour'), 
           out_name = c('q(m3/s)', 'evap(mm)', 'tair',
                        'tmax', 'tmin', 'wspd(km/hr)') )

# show all the loaded data
guido %>%
  hm_show()
```

now let’s day that we want a report of missing data and basic statistics
of the daily discharge and to make a dynamic plot to analyze the series

``` r
# we want to know something about mean daily streamflow discharge
guido %>% 
  hm_report(slot_name = 'qd', col_name = 'q(m3/s)')
  
  
# plot discharge
guido %>% 
  hm_plot(slot_name = 'qd', col_name = list( 'q(m3/s)' ), 
          interactive = TRUE, 
          line_color = 'dodgerblue',
          y_lab = 'Q(m3/s)'   ) 
```

we realize that we need to smooth the series and remove some mistrustful
periods

``` r
# smooth the series 
guido <- 
   guido %>% 
       hm_mutate(slot_name = 'qd', FUN = mov_avg, 
              k = 3, out_name = 'q_smooth') 

# remove mistrustful measurements 
guido <- 
  guido %>%
    hm_mutate(slot_name = 'qd', FUN = set_value,
              col_name = 'q_smooth', out_name = 'q_set',
              value = rep(NA_real_, 2),
              from = c('1965-08-09', '1974-06-26'),
              to = c('1965-08-25', '1974-07-04') )
```

now we plot the three versions so see the effect of the proposed
modifications. Then we export the object, save the table and the plot.

``` r
guido %>% 
  hm_plot(slot_name = 'qd', 
          col_name = list( c('q(m3/s)', 'q_set') ), 
          interactive = TRUE, 
          line_color = c('dodgerblue', 'forestgreen'),
          y_lab = 'Q(m3/s)',
          legend_lab = c('original', 'set')) 
          
# save the hydromet object
saveRDS(object = guido, path = "my_path/guido.rds")

# extract the table and save it
guido_table <- 
  guido %>% 
  hm_get(slot_name = "qd")

wirte.csv(x = guido_table,
          file = "my_path/guido_qd.csv",
          row.names = FALSE)

# create a ggplot2 object and save it
gg_guido <- 
  guido %>% 
  hm_plot(slot_name = 'qd', 
          col_name = list( c('q(m3/s)', 'q_set') ), 
          interactive = FALSE, 
          line_color = c('dodgerblue', 'forestgreen'),
          y_lab = 'Q(m3/s)', x_lab = "",
          legend_lab = c('original', 'set')) 
          
ggsave(filename = "guido_qd.png", 
       plot = gg_guido,
       path = "my_path",
       width = 5, 
       height = 3,
       units = "cm")
```

We are also interested on monthly mean discharge, so we need to
aggregate the daily discharge

``` r
# aggregate and relocate
guido <- 
  guido %>%
  hm_agg(slot_name = 'qd', col_name = 'q_set', 
         fun = 'mean', 
         period = 'monthly', 
         out_name = 'q_mean', 
         relocate = 'qm' )
```

Try to plot the mean monthly discharge…

## Installation

To install it, use:

``` r
install.packages("hydrotoolbox")
```

## Note

This package is an improved version of `hydroToolkit`. After using it in
a big project, I realize that classes could be reduced and that many
aspects of the arguments should be rewritten to make the package more
user-friendly. For this new package (`hydrotoolbox`) I also decided to
follow the *tidyverse style*, so people used to the *tidyverse* syntax
should feel comfortable on using `hydrotoolbox`.

In summary `hydroToolkit` will be achieved soon. If you were using it, I
strongly recommend you to move to `hydrotoolbox`.
