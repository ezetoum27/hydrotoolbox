---
title: "cran-comments"
author: "Ezequiel Toum"
date: "2023-04-12"
output: html_document
---

## Package update
This is a package update.

## Test environments
* local ubuntu 20.04, R 4.2.1
* win-builder (devel, release and old-release)

## R CMD check results
There were no ERRORs or WARNINGs. 

There was one NOTE:

* checking installed package size ... NOTE
    installed size is 11.8Mb
    sub-directories of 1Mb or more:
      extdata   5.5Mb
      libs      5.5Mb
  
I need the raw data (extdata) to run real
world examples, since the package was design
to work with hydro-meteorological data from 
national agencies, private companies and research
groups from Argentina and Chile.

## spell_check()

* Vignettes are bilingual (english and spanish).
There are **no** misspelled words.
